/* ============================
    Preloader
    --------------------------- */
$(window).on('load', function () { // makes sure that your website is fully loaded
    $('#status').fadeOut();
    $('#preloader').delay(250).fadeOut('Slow');
});

/* ============================
    Team
    --------------------------- */
$(function () {
    $("#team-members").owlCarousel({
        items: 2,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });
});
/* ============================
    Progress Bars
    --------------------------- */
$(function () {
    $("#progress-elements").waypoint(function () {
        $(".progress-bar").each(function () {
            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 1000);
        });
        $(this).destroy();
    }, {
        offset: 'bottom-in-view'
    });
});

/* ============================
    Statement Selection Carousel
    --------------------------- */
$(function () {
    $("#statement-selection").owlCarousel({
        items: 1,
        autoplay: true,
        smartSpped: 1000,
        loop: true,
        autoplayHoverPause: true,
        dots:false
    });
});

/* ============================
    Responsive Tabs
    --------------------------- */
$(function(){
    $("#services-tabs").responsiveTabs({
        animation: 'slide'
    });
});

/* ============================
    Portfolio
    --------------------------- */

// Initialize isotope
$(window).on('load', function () {
    $("#isotope-container").isotope({});
    // Filter items on button click
    $("#isotope-filter").on('click', 'button', function () {
        // get filter value    
        var filterValue = $(this).attr('data-filter');
        $("#isotope-container").isotope({
            filter: filterValue
        });
        $("#isotope-filter").find('.active').removeClass('active');
        $(this).addClass('active')
    });
});

$(function () {
    $("#portfolio-wrapper").magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        }
    });
});

/* ============================
   Testimonial Carousel
    --------------------------- */
$(function () {
    $("#testimonial-slider").owlCarousel({
        items: 1,
        autoplay: false,
        smartSpped: 1000,
        loop: true,
        autoplayHoverPause: true,
        dots:false,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });
});

/* ============================
   stats Counter
    --------------------------- */
$(function () {
    $(".counter").counterUp({
        delay: 10,
        time: 2000
    });
});


/* ============================
   Clients Carousel
    --------------------------- */
$(function () {
    $("#client-slide").owlCarousel({
        items: 6,
        autoplay: true,
        smartSpped: 1000,
        loop: true,
        autoplayHoverPause: true,
        dots:false,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });
});

/* ============================
    Google map
    --------------------------- */
$(window).on('load', function () {
    var adressstring = '230 Broadway, NY, Newyork 10007, USA';
    var mylatlng = {lat: 7.376282, lng: 3.895719};
    
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: mylatlng                          
    });
    // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: mylatlng, map: map});
    
    var Infowindow = new google.maps.Infowindow({
        
    content: adressstring
    });
    
    marker.addListener('click', function(){
        infowindow.open(map, marker);
    });
});

$(function (){
    
    // show navigation on page load
    showHideNav();
    
    $(window).scroll(function(){
        
    // show navigation on scroll
    showHideNav();
   });
    
    function showHideNav(){
        if ($(window).scrollTop () > 50) {
        // Show white nav
        $("nav").addClass("white-nav-top");
        //Show dark logo
        $(".navbar-brand img").attr("src", "img/logo/weblogo.png");
    } else{
        // Hide white nav
        $("nav").removeClass("white-nav-top");
        //Show logo
        $(".navbar-brand img").attr("src", "img/logo/weblogo-dark.png");
    }    
    }
});
  
  
  
  
  
  
  
  
  
  











